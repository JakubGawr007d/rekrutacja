import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { TeamData } from '../domain-model/models/team-members';

@Injectable()
export class TeamMembersState {
  private stateSubject = new BehaviorSubject<TeamData[]>([]);
  state$ = this.stateSubject.asObservable();

  get state(): TeamData[] {
    return this.stateSubject.value;
  }

  select<K>(mapFn: (state: TeamData[]) => K): Observable<K> {
    return this.state$.pipe(
      map((state: TeamData[]) => mapFn(state)),
      distinctUntilChanged()
    );
  }

  setState(newState: TeamData[]): void {
    this.stateSubject.next([...this.state, ...newState]);
  }
}

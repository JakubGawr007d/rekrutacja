import { Inject, Injectable } from '@angular/core';
import { GET_TEAM_MEMBERS, GetTeamData } from '../infrastructure/tokens';
import {
  MemberInformation,
  TeamData,
} from '../domain-model/models/team-members';
import { Observable } from 'rxjs';
import { TeamMembersState } from './team-members-state';
import { take } from 'rxjs/operators';

@Injectable()
export class TeamMembersFacade {
  teamMembers$ = this.teamMembersState.state$;
  title$ = this.teamMembersState.select((stateValues) =>
    stateValues.flatMap(({ attributes }) => attributes.title).join()
  );
  constructor(
    @Inject(GET_TEAM_MEMBERS) private teamDataService: GetTeamData<TeamData[]>,
    private teamMembersState: TeamMembersState
  ) {}

  getAttributesList(): Observable<MemberInformation[]> {
    return this.teamMembersState.select((stateValues) => {
      return stateValues.flatMap(({ attributes }) =>
        Object.values(attributes.memberCards)
      );
    });
  }

  loadTeamData(): void {
    this.teamDataService
      .loadTeamData()
      .pipe(take(1))
      .subscribe((data) => this.teamMembersState.setState(data));
  }
}

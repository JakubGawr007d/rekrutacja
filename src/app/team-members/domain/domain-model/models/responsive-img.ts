export enum SRC_SETS {
  w200 = 'w200',
  w400 = 'w400',
  w1080 = 'w1080',
  w1920 = 'w1920',
  w2560 = 'w2560',
}

export type ImageUrlSizes = { [key in SRC_SETS]: string };

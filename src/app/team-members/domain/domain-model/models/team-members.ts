import { ImageUrlSizes } from './responsive-img';

export enum MEMBER_ORDER {
  'first' = 'first',
  'second' = 'second',
  'third' = 'third',
}

export interface TeamMember {
  title: string;
  description: string;
  link: string;
  text: string;
}

export interface MemberInformation {
  imageUrl: ImageUrlSizes;
  block: TeamMember;
}

type MemberCards = { [key in MEMBER_ORDER]: MemberInformation };

export interface Attributes {
  title: string;
  memberCards: MemberCards;
}

export interface TeamData {
  type: string;
  id: string;
  attributes: Attributes;
}

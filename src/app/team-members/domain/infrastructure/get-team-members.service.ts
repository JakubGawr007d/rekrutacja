import { Injectable } from '@angular/core';
import { GetTeamData } from './tokens';
import { TeamData } from '../domain-model/models/team-members';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class GetTeamMembersService implements GetTeamData<TeamData[]> {
  constructor(private httpClient: HttpClient) {}

  loadTeamData(): Observable<TeamData[]> {
    return this.httpClient
      .get<{ data: TeamData[] }>(`${environment.api}/task/index.json`)
      .pipe(map((response) => response.data));
  }
}

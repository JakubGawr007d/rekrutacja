import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { TeamData } from '../domain-model/models/team-members';

export interface GetTeamData<T> {
  loadTeamData(): Observable<T>;
}

export const GET_TEAM_MEMBERS = new InjectionToken<TeamData[]>(
  'GET_TEAM_MEMBERS'
);

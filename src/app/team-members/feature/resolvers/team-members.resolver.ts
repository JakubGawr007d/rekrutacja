import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { TeamMembersFacade } from '../../domain/application/team-members.facade';

@Injectable()
export class TeamMembersResolve implements Resolve<boolean> {
  constructor(private teamMembersFacade: TeamMembersFacade) {}

  resolve(route: ActivatedRouteSnapshot): boolean {
    this.teamMembersFacade.loadTeamData();
    return true;
  }
}

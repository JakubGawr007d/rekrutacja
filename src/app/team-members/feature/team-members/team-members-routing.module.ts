import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamMembersComponent } from './team-members.component';
import { TeamMembersResolve } from '../resolvers/team-members.resolver';

const routes: Routes = [
  {
    path: '',
    component: TeamMembersComponent,
    resolve: { data: TeamMembersResolve },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeamMembersRoutingModule {}

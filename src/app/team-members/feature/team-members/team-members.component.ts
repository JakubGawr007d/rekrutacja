import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TeamMembersFacade } from '../../domain/application/team-members.facade';
import { Observable } from 'rxjs';
import { MemberInformation } from '../../domain/domain-model/models/team-members';

@Component({
  selector: 'app-team-members',
  templateUrl: './team-members.component.html',
  styleUrls: ['./team-members.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeamMembersComponent implements OnInit {
  attributesList$!: Observable<MemberInformation[]>;
  sectionTitle$!: Observable<string>;
  constructor(private teamMembersFacade: TeamMembersFacade) {}

  ngOnInit(): void {
    this.attributesList$ = this.teamMembersFacade.getAttributesList();
    this.sectionTitle$ = this.teamMembersFacade.title$;
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamMembersRoutingModule } from './team-members-routing.module';
import { GET_TEAM_MEMBERS } from '../../domain/infrastructure/tokens';
import { GetTeamMembersService } from '../../domain/infrastructure/get-team-members.service';
import { TeamMembersComponent } from './team-members.component';
import { HttpClientModule } from '@angular/common/http';
import { TeamMembersFacade } from '../../domain/application/team-members.facade';
import { TeamMembersState } from '../../domain/application/team-members-state';
import { TeamMembersResolve } from '../resolvers/team-members.resolver';
import { CardModule } from '../../../ui/card/card.module';

@NgModule({
  declarations: [TeamMembersComponent],
  imports: [
    CommonModule,
    TeamMembersRoutingModule,
    HttpClientModule,
    CardModule,
  ],
  providers: [
    {
      provide: GET_TEAM_MEMBERS,
      useClass: GetTeamMembersService,
    },
    TeamMembersFacade,
    TeamMembersState,
    TeamMembersResolve,
  ],
  exports: [TeamMembersComponent],
})
export class TeamMembersModule {}

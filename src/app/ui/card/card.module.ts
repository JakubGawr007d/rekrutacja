import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card.component';
import { CardTitleDirective } from './directives/card-title.directive';
import { CardDescriptionDirective } from './directives/card-description.directive';
import { CardContactDirective } from './directives/card-contact.directive';
import { CardFooterDirective } from './directives/card-footer.directive';
import { CardImageDirective } from './directives/card-image.directive';

@NgModule({
  declarations: [
    CardComponent,
    CardTitleDirective,
    CardDescriptionDirective,
    CardContactDirective,
    CardFooterDirective,
    CardImageDirective,
  ],
  imports: [CommonModule],
  exports: [
    CardComponent,
    CardTitleDirective,
    CardDescriptionDirective,
    CardContactDirective,
    CardFooterDirective,
    CardImageDirective,
  ],
})
export class CardModule {}

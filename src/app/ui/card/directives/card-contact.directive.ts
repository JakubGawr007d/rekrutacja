import { Directive } from '@angular/core';

@Directive({
  selector: 'app-card-contact, [app-card-contact]',
  host: { class: 'c-card__contact' },
})
export class CardContactDirective {
  constructor() {}
}

import { Directive } from '@angular/core';

@Directive({
  selector: 'app-card-description, [app-card-description]',
  host: { class: 'c-card__description' },
})
export class CardDescriptionDirective {
  constructor() {}
}

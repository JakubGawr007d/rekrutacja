import { Directive } from '@angular/core';

@Directive({
  selector: 'app-card-footer, [app-card-footer]',
  host: { class: 'c-card__footer' },
})
export class CardFooterDirective {
  constructor() {}
}

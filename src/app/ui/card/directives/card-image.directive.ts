import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import {
  ImageUrlSizes,
  SRC_SETS,
} from '../../../team-members/domain/domain-model/models/responsive-img';

@Directive({
  selector: 'app-card-image, [app-card-image]',
  host: { class: 'c-card__img' },
})
export class CardImageDirective implements OnInit {
  @Input() imgSrcsets!: ImageUrlSizes;
  constructor(private renderer: Renderer2, private elementRef: ElementRef) {}

  ngOnInit(): void {
    if (this.imgSrcsets) {
      this.renderer.setAttribute(
        this.elementRef.nativeElement,
        'srcset',
        this.attachImgSizes(this.imgSrcsets)
      );
    }
  }
  private attachImgSizes(value: ImageUrlSizes): string {
    return Object.values(SRC_SETS)
      .map((key) => {
        const size = key.replace('w', '');
        return `${value[key]} ${size}w`;
      })
      .join(',');
  }
}

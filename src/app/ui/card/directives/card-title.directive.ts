import { Directive } from '@angular/core';

@Directive({
  selector: 'app-card-title, [app-card-title]',
  host: { class: 'c-card__title' },
})
export class CardTitleDirective {
  constructor() {}
}
